#!/bin/bash

set -e

if [ "$1" = 'init' ]; then
    cd /usr/local/share/nextcloud
    mkdir -p /var/lib/nextcloud/{data,config}
    cp /etc/nextcloud/defaults.config.php /var/lib/nextcloud/config/defaults.config.php
    cp /usr/local/share/nextcloud/.htaccess.orig /var/lib/nextcloud/.htaccess
    cp /usr/local/share/nextcloud/.htaccess.config.orig /var/lib/nextcloud/config/.htaccess

    if [ ! -f /var/lib/nextcloud/config/config.php ]; then
        echo "Initializing..."
        php occ maintenance:install -n --database "mysql" --database-host ${DB_HOST} --database-name ${DB_NAME} --database-user ${DB_USER} --database-pass ${DB_PASS} --admin-user ${OC_USER} --admin-pass ${OC_PASS} --data-dir /var/lib/nextcloud/data 
        echo "Initialization complete."
    else
        php occ upgrade
        php occ user:resetpassword ${OC_USER} --password-from-env
        php occ maintenance:repair
        echo "Updates complete."
    fi
    php occ app:enable user_ldap
    for app in ${NC_EAPPS}; do
        php occ app:enable ${app}
    done
    for app in ${NC_DAPPS}; do
        php occ app:disable ${app}
    done
    php occ maintenance:update:htaccess
    exec echo "System ready."
fi

if [ "$1" = 'nextcloud' ]; then
    cd /usr/local/share/nextcloud
    exec php occ log:watch
fi

if [ "$1" = 'apache2' ]; then
    exec /usr/sbin/apache2 -DFOREGROUND
fi

if [ "$1" = 'cron' ]; then
    exec yacron
fi

exec "$@"
