#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=nextcloud
ENV APP_GROUP=nextcloud

ENV NEXTCLOUD_VERSION=28.0.10
ENV YACRON_VERSION=0.19.0
ENV NC_IAPPS="appointments calendar contacts cookbook deck drawio epubviewer forms groupfolders mail notes polls richdocuments spreed tasks twofactor_webauthn"

ENV VENV_PATH=/usr/local/share/yacron

# Set base dependencies
ENV BASE_PKGS apache2 bzip2 libapache2-mod-php php-bcmath php-bz2 php-curl php-gd php-gmp php-imagick php-imap \
              php-intl php-ldap php-mbstring php-mysql php-opcache php-redis php-sqlite3 php-xml php-zip python3

# Update users and groups
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -s /usr/sbin/nologin

# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV BUILD_DEPS wget python3-venv

RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

# Run build
WORKDIR /usr/src

RUN python3 -m venv ${VENV_PATH} --copies
RUN ${VENV_PATH}/bin/pip3 install --upgrade pip
RUN ${VENV_PATH}/bin/pip3 install yacron==${YACRON_VERSION}

RUN wget -nv https://download.nextcloud.com/server/releases/nextcloud-${NEXTCLOUD_VERSION}.tar.bz2
RUN tar -jxf nextcloud-${NEXTCLOUD_VERSION}.tar.bz2
RUN mv nextcloud /usr/local/share/nextcloud

WORKDIR /usr/local/share/nextcloud
RUN php occ maintenance:install --admin-user install --admin-pass `openssl rand -base64 32`
RUN sed -i "s|);|  'allow_local_remote_servers' => true,\n);|" config/config.php
RUN for app in ${NC_IAPPS}; do php occ app:install ${app}; done
RUN mv /usr/local/share/nextcloud/.htaccess /usr/local/share/nextcloud/.htaccess.orig
RUN mv /usr/local/share/nextcloud/config/.htaccess /usr/local/share/nextcloud/.htaccess.config.orig
RUN rm -Rf /usr/local/share/nextcloud/config
RUN rm -Rf /usr/local/share/nextcloud/data
RUN chown -R ${APP_USER}:${APP_GROUP} /usr/local/share/nextcloud
RUN chmod +x /usr/local/share/nextcloud/occ
RUN chmod +x /usr/local/share/nextcloud/apps/mail/vendor/nextcloud/kitinerary-bin/bin/kitinerary-extractor


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY etc/ /etc/
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

ENV OC_USER="ncadmin"
ENV OC_PASS=""
ENV DB_HOST=""
ENV DB_NAME="nextcloud"
ENV DB_USER="nextcloud"
ENV DB_PASS=""
ENV PATH=${PATH}:${VENV_PATH}/bin

RUN a2enmod rewrite \
 && a2enmod headers \
 && a2enmod env \
 && a2enmod mime \
 && a2dismod reqtimeout \
 && rm /etc/apache2/conf-available/other-vhosts-access-log.conf \
 && mkdir -p /run/apache2 \
 &&	chown ${APP_USER}:${APP_GROUP} /run/apache2 \
 && mkdir -p /var/lib/nextcloud/config \
 && chown -R ${APP_USER}:${APP_GROUP} /var/lib/nextcloud \
 && ln -s /var/lib/nextcloud/config /usr/local/share/nextcloud/config \
 && ln -s /var/lib/nextcloud/.htaccess /usr/local/share/nextcloud/.htaccess

EXPOSE 8080/tcp
VOLUME ["/var/lib/nextcloud"]

USER $APP_USER
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["apache2"]
